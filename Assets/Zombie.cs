﻿using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
	NavMeshAgent agent;
	GameObject currentRamp;
	Vector3 target = Vector3.zero;

	int health = 10;
	public bool movingIn = false;

	private void Awake ()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	public void GiveTarget(Vector3 newTarget)
	{
		agent.SetDestination(newTarget);
	}

	public void Hit()
	{
		health--;
		if(health <= 0)
		{
			Die();
		}
	}

	void Die ()
	{
		SendMessageUpwards("ZombieDied");
		health = 0;
		agent.SetDestination(transform.position);
		agent.isStopped = true;
		if(currentRamp)
		{
			currentRamp.GetComponent<Ramp>().Build();
		}
		Destroy(gameObject, 1f);
	}

	void OnTriggerEnter (Collider other)
	{
		if(other.tag == "Ramp")
		{
			currentRamp = other.gameObject;
			if(!other.GetComponent<OffMeshLink>().activated)
			{
				Invoke("Die", 1f);
			}
		}

		if(other.tag == "Finish")
		{
			agent.SetDestination(Vector3.zero);
		}
		
	}

	void OnTriggerExit (Collider other)
	{
		if(other.tag == "Ramp")
		{
			currentRamp = null;
		}
	}
}
