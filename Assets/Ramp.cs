﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class Ramp : MonoBehaviour
{
	public int totalStages = 10;
	public int stagesToBuild = 8;
	[SerializeField] int rampStage;

	ZombieController zControl;
	BoxCollider coll;
	Vector3 collCenter;
	Vector3 collSize;

    void Start()
    {
		zControl = FindObjectOfType<ZombieController>();
		coll = GetComponent<BoxCollider>();
		GetComponent<OffMeshLink>().activated = false;
	}

	public void Build ()
	{
		if(rampStage < totalStages)
		{
			rampStage++;
			MoveCapsule(1);
			if(rampStage >= stagesToBuild)
			{
				GetComponent<OffMeshLink>().activated = true;
			}
		}
	}

	public void Dismantle ()
	{
		if(rampStage > 0)
		{
			rampStage--;
			MoveCapsule(-1);
			if(rampStage < stagesToBuild)
			{
				GetComponent<OffMeshLink>().activated = false;
			}
		}
	}

	void MoveCapsule (int direction)
	{
		if(rampStage <= stagesToBuild)
		{
			GameObject capsule = GetComponentInChildren<MeshFilter>().gameObject;
			capsule.transform.position += new Vector3(0, direction, 0);
		}
	}

	void OnDrawGizmos ()
	{
		collCenter.Set(0, 0.5f, -7);
		collSize.Set(6, 1, 6);

		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(transform.position + collCenter, collSize);
	}
}
