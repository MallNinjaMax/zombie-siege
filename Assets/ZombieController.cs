﻿using UnityEngine;

public class ZombieController : MonoBehaviour
{
	public int currentZombies;
	public int maxZombies = 100;
	public int zombiesKilled;
	public GameObject zombiePrefab;
	ZombieSpawner[] spawners;
	[SerializeField] ZombieTarget[] targets;

	void Start()
    {
		spawners = FindObjectsOfType<ZombieSpawner>();
		targets = FindObjectsOfType<ZombieTarget>();
		
		InvokeRepeating("SpawnStragglers", 2f, 0.2f);
	}

    public void ZombieDied()
	{
		currentZombies--;
		zombiesKilled++;
	}

	void SpawnStragglers ()
	{
		bool willSpawn = Random.Range(0, 10) > 0.5f; // TODO magic numbers
		if(willSpawn && currentZombies < maxZombies)
		{
			currentZombies++;
			Transform spawn = spawners[Random.Range(0, spawners.Length)].transform;
			Transform target = targets[Random.Range(0, targets.Length)].transform;
			GameObject zombie = Instantiate(zombiePrefab, spawn.position, spawn.rotation, transform);
			zombie.GetComponent<Zombie>().GiveTarget(target.position);
		}
	}
}
