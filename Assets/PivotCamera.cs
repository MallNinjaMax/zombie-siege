﻿using UnityEngine;

public class PivotCamera : MonoBehaviour
{
	public float cameraRotationSpeed;

	public Vector3 camZoomedInPos;
	public Vector3 camZoomedOutPos;

	float scrollAxis;
	 
	void Start ()
	{

	}

	void Update ()
	{
		scrollAxis = Input.GetAxis("Mouse ScrollWheel");

		if(Input.GetButton("Fire2"))
		{
			float direction = Input.mousePosition.x - (Screen.width / 2);
			transform.Rotate(new Vector3(0, (-direction * cameraRotationSpeed) * Time.deltaTime, 0));
		}

		if(scrollAxis > 0)
		{
			Camera.main.transform.localPosition = camZoomedInPos;
		}
		else if(scrollAxis < 0)
		{
			Camera.main.transform.localPosition = camZoomedOutPos;
		}
	}
}
